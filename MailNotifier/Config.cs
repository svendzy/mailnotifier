﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Security.Cryptography;

namespace MailNotifier
{
    [Serializable()]
    public class Config
    {
        private AccountList m_Accounts;
        private static string configFile;

        private Config()
        {
            m_Accounts = new AccountList();

            string configFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "MailNotifier");
            configFile = Path.Combine(configFolder, "config.xml");
        }

        public AccountList Accounts
        {
            get
            {
                return m_Accounts;
            }
        }

        public static Config Load()
        {
            Config config = new Config();
            if (File.Exists(configFile))
            {
                using (FileStream fs = new FileStream(configFile, FileMode.Open))
                {
                    BinaryFormatter bf = new BinaryFormatter();
                    config = bf.Deserialize(fs) as Config;
                }
            }
            return config;
        }

        public void Save()
        {
            string configFolder = Path.GetDirectoryName(configFile);
            if (!Directory.Exists(configFolder))
                Directory.CreateDirectory(configFolder);

            using (FileStream fs = File.OpenWrite(configFile))
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(fs, this);
            }
        }
    }
}
