﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MailNotifier
{
    public interface IObserver
    {
        void Notify(AccountStatus accountStatus);
    }
}
