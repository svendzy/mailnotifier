﻿using System;

namespace MailNotifier
{
    public class AccountStatus
    {
        private string m_Email;
        private AccountType m_AccountType;
        private int m_MessageCount;
        private int m_OctetSize;

        public string Email
        {
            get
            {
                return m_Email;
            }
            set
            {
                m_Email = value;
            }
        }

        public AccountType AccountType
        {
            get
            {
                return m_AccountType;
            }
            set
            {
                m_AccountType = value;
            }
        }

        public int MessageCount
        {
            get
            {
                return m_MessageCount;
            }
            set
            {
                m_MessageCount = value;
            }
        }

        public int OctetSize
        {
            get
            {
                return m_OctetSize;
            }
            set
            {
                m_OctetSize = value;
            }
        }
    }
}
