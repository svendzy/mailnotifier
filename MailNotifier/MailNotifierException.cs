﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MailNotifier
{
    public class MailNotifierException : System.Exception
    {
        public MailNotifierException() : base() { }
        public MailNotifierException(string message) 
            : base(message) { }
        public MailNotifierException(string message, Exception ex) 
            : base(message, ex) { }
    }
}
