﻿using System;
using System.Diagnostics;
using Microsoft.Win32;

namespace MailNotifier
{
    public static class DefaultMailClient
    {
        public static void Run()
        {
            // Get Default Email Client from Registry
            string szDefault = (string)Registry.GetValue("HKEY_CURRENT_USER\\SOFTWARE\\Clients\\Mail", "", "");
            if (szDefault != String.Empty)
            {
                // Run Default email client
                string szCommand = (string)Registry.GetValue(
                    String.Format("HKEY_LOCAL_MACHINE\\SOFTWARE\\Clients\\Mail\\{0}\\shell\\open\\command", szDefault), "", "");
                if (szCommand != String.Empty)
                {
                    Process reader = new Process();
                    string szFilename = szCommand.Split(new string[] { "\"" }, StringSplitOptions.RemoveEmptyEntries)[0];
                    reader.StartInfo.FileName = szFilename;
                    reader.Start();
                }
            }
        }
    }
}
