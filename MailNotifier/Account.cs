﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace MailNotifier
{
    [Serializable()]
    public abstract class Account : IObservable
    {
        private string m_Email;
        private string m_Host;
        private int m_Port;
        private string m_Username;
        private string m_Password;
        private int m_CheckIntervalInMinutes = 5;
        [NonSerialized]
        private List<IObserver> m_Observers = null;
        [NonSerialized]
        private System.Threading.Timer m_Timer = null;

        public string Email
        {
            get
            {
                return m_Email;
            }
            set
            {
                m_Email = value;
            }
        }

        public string Host
        {
            get
            {
                return m_Host;
            }
            set
            {
                m_Host = value;
            }
        }

        public int Port
        {
            get
            {
                return m_Port;
            }
            set
            {
                m_Port = value;
            }
        }

        public string Username
        {
            get
            {
                return m_Username;
            }
            set
            {
                m_Username = value;
            }
        }

        public string Password
        {
            get
            {
                return m_Password;
            }
            set
            {
                m_Password = value;
            }
        }

        public int CheckIntervalInMinutes
        {
            get
            { 
                return m_CheckIntervalInMinutes;
            }
            set
            { 
                m_CheckIntervalInMinutes = value;
            }
        }

        private void _TimerCallback(object args)
        {
            try
            {
                AccountStatus status = GetStatus();
                if (status != null)
                {
                    Notify(status);
                }
            }
            catch (System.Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        public void SubscribeNotify(IObserver observer)
        {
            if (m_Observers == null)
                m_Observers = new List<IObserver>();
            m_Observers.Add(observer);
        }

        public void Notify(AccountStatus accountStatus)
        {
            if (m_Observers != null)
            {
                for (int nObserver = 0; nObserver < m_Observers.Count; nObserver++)
                {
                    m_Observers[nObserver].Notify(accountStatus);
                }
            }
        }

        public void StartNotify()
        {
            if (m_Timer == null)
                m_Timer = new Timer(new TimerCallback(_TimerCallback), null, Timeout.Infinite, Timeout.Infinite);
            m_Timer.Change(m_CheckIntervalInMinutes * 60000, m_CheckIntervalInMinutes * 60000);
        }

        public void StopNotify()
        {
            if (m_Timer != null)
                m_Timer.Change(Timeout.Infinite, Timeout.Infinite);
        }

        public void Dispose()
        {
            if (m_Timer != null)
            {
                m_Timer.Change(Timeout.Infinite, Timeout.Infinite);
                m_Timer.Dispose();
            }
        }

        public abstract AccountType Type { get; }
        protected abstract AccountStatus GetStatus();
    }
}
