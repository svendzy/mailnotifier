﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MailNotifier
{
    public partial class FrmAccountSetup : Form
    {
        public FrmAccountSetup()
        {
            InitializeComponent();
        }

        public AccountType AccountType
        {
            get
            {
                AccountType accountType = AccountType.Standard;
                switch (cbxAccountType.SelectedIndex)
                {
                    case 0: accountType = AccountType.Standard;
                        break;
                    case 1: accountType = AccountType.GMail;
                        break;
                }
                return accountType;
            }
        }

        public string Email
        {
            get { return txtEmail.Text; }
        }

        public string Host
        {
            get { return txtHost.Text; }
        }

        public int Port
        {
            get { return Convert.ToInt32(nuPort.Value); }
        }

        public string Username
        {
            get { return txtUsername.Text; }
        }

        public string Password
        {
            get { return txtPassword.Text; }
        }

        public int CheckIntervalInMinutes
        {
            get { return Convert.ToInt32(nuCheckIntervalInMinutes.Value); }
        }

        private void FrmSetupAccount_Load(object sender, EventArgs e)
        {
            cbxAccountType.SelectedIndex = 0;
        }

        private void cbxAccountType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxAccountType.SelectedIndex != -1)
            {
                switch (cbxAccountType.SelectedIndex)
                {
                    case 0: 
                        txtEmail.Text = "user@domain.xxx";
                        txtHost.Text = "domain.xxx";
                        nuPort.Value = 110;
                        break;
                    case 1:
                        txtEmail.Text = "user@gmail.com";
                        txtHost.Text = "imap.gmail.com";
                        nuPort.Value = 993;
                        break;
                }
            }
        }

    }
}
