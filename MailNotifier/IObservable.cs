﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MailNotifier
{
    public interface IObservable
    {
        void StartNotify();
        void StopNotify();
        void SubscribeNotify(IObserver observer);
    }
}
