﻿using System;
using System.Security.Cryptography;

namespace MailNotifier
{
    public static class DataEncryptor
    {
        static byte[] s_aditionalEntropy = { 1, 6, 7, 8, 5 };

        public static byte[] Encrypt(byte[] data  )
        {
            return ProtectedData.Protect(data, s_aditionalEntropy,DataProtectionScope.CurrentUser);
        }

        public static byte[] Decrypt(byte[] data)
        {
            return ProtectedData.Unprotect(data, s_aditionalEntropy, DataProtectionScope.CurrentUser);
        }
    }
}
