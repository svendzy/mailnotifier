﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MailNotifier
{
    [Serializable()]
    public class StandardAccount : Account
    {
        public StandardAccount()
        {
            Email = String.Empty;
            Host = String.Empty;
            Port = 110;
            Username = String.Empty;
            Password = String.Empty;
        }

        public override AccountType Type
        {
            get { return AccountType.Standard; }
        }

        protected override AccountStatus GetStatus()
        {
            NetworkSocket socket = null;

            try
            {
                socket = new NetworkSocket(Host, Port);

                // Read Welcome message
                string result = socket.ReadLine(Encoding.ASCII);

                // Execute USER command
                socket.WriteLine(String.Format("USER {0}", Username), Encoding.ASCII);
                result = socket.ReadLine(Encoding.ASCII);
                if (!result.StartsWith("+OK"))
                    throw new MailNotifierException("USER command failed");

                // Execute PASS command
                socket.WriteLine(String.Format("PASS {0}", Crypto.DecryptStringAES(Password)), Encoding.ASCII);
                result = socket.ReadLine(Encoding.ASCII);
                if (!result.StartsWith("+OK"))
                    throw new MailNotifierException("PASS command failed");

                // Execute STAT command
                socket.WriteLine("STAT", Encoding.ASCII);
                result = socket.ReadLine(Encoding.ASCII);
                if (!result.StartsWith("+OK"))
                    throw new MailNotifierException("STAT command failed");

                string[] result_split = result.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                AccountStatus status = new AccountStatus();
                status.Email = String.Copy(Email);
                status.AccountType = Type;
                status.MessageCount = Convert.ToInt32(result_split[1]);
                status.OctetSize = Convert.ToInt32(result_split[2]);

                // Execute QUIT command
                socket.WriteLine("QUIT", Encoding.ASCII);
                result = socket.ReadLine(Encoding.ASCII);
                if (!result.StartsWith("+OK"))
                    throw new MailNotifierException("QUIT command failed");
                return status;
            }
            finally
            {
                if (socket != null) socket.Close();
            }  
        }
    }
}
