﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MailNotifier
{
    [Serializable()]
    public class GMailAccount : Account
    {
        private int _prefixNum = 0;

        public GMailAccount()
        {
            Email = "user@gmail.com";
            Host = "imap.gmail.com";
            Port = 993;
            Username = String.Empty;
            Password = String.Empty;
        }

        public override AccountType Type
        {
            get { return AccountType.GMail; }
        }

        private string Prefix()
        {
            _prefixNum++;
            return "a" + _prefixNum.ToString().PadLeft(4, '0');
        }

        protected override AccountStatus GetStatus()
        {
            EncryptedNetworkSocket socket = null;

            try
            {
                socket = new EncryptedNetworkSocket(Host, Port, "imap.gmail.com");

                string[] result_arr;

                // Read Welcome message
                string result = socket.ReadLine(Encoding.ASCII);

                // Execute LOGIN command
                string _prefix = Prefix();
                socket.WriteLine(String.Format("{0} LOGIN {1} {2}", _prefix, Username, Crypto.DecryptStringAES(Password)), Encoding.ASCII);
                result_arr = socket.ReadResponse(Encoding.ASCII, _prefix);
                if (!result_arr[result_arr.Length-1].StartsWith(_prefix + " OK"))
                    throw new MailNotifierException("LOGIN command failed");
                
                // Execute SELECT INBOX command

                _prefix = Prefix();
                socket.WriteLine(String.Format("{0} SELECT INBOX", _prefix), Encoding.ASCII);
                result_arr = socket.ReadResponse(Encoding.ASCII, _prefix);
                if (!result_arr[result_arr.Length - 1].StartsWith(_prefix + " OK"))
                    throw new MailNotifierException("SELECT INBOX command failed");


                _prefix = Prefix();
                socket.WriteLine(String.Format("{0} SEARCH UNSEEN", _prefix), Encoding.ASCII);
                result_arr = socket.ReadResponse(Encoding.ASCII, _prefix);
                if (!result_arr[result_arr.Length - 1].StartsWith(_prefix + " OK"))
                    throw new MailNotifierException("STATUS INBOX command failed");

                int messageCount = GetUnreadMessageCount(result_arr);

                AccountStatus status = new AccountStatus();
                status.Email = String.Copy(Email);
                status.AccountType = Type;
                status.MessageCount = messageCount;
                status.OctetSize = 0;

                // Execute LOGOUT command
                _prefix = Prefix();
                socket.WriteLine(String.Format("{0} LOGOUT", _prefix), Encoding.ASCII);
                result_arr = socket.ReadResponse(Encoding.ASCII, _prefix);
                if (!result_arr[result_arr.Length - 1].StartsWith(_prefix + " OK"))
                    throw new MailNotifierException("LOGOUT command failed");
                return status;
            }
            finally
            {
                if (socket != null) socket.Close();
            }
        }

        private int GetUnreadMessageCount(string[] searchResponse)
        {
            int messageCount = -1;
            if ((searchResponse != null) && (searchResponse.Length == 2))
            {
                string _searchResponse = searchResponse[0];
                string[] resp = _searchResponse.Split(new char[] { ' ' });
                messageCount = resp.Length - 2;
            }
            return messageCount;
        }
    }
}
