using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Net.Security;
using System.Text;

namespace MailNotifier
{
    public class EncryptedNetworkSocket
    {
        private Socket m_Socket;
        private NetworkStream m_NetworkStream;
        private SslStream m_Ssl;

        public EncryptedNetworkSocket(string host, int port, string authenticate)
        {
            m_Socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            m_Socket.Connect(host, port);
            m_NetworkStream = new NetworkStream(m_Socket, false);
            m_Ssl = new SslStream(m_NetworkStream);
            m_Ssl.AuthenticateAsClient(authenticate);
        }

        public void WriteLine(string s, Encoding enc)
        {
            string str = s + "\r\n";
            byte[] buffer = enc.GetBytes(str);
            m_Ssl.Write(buffer);
        }

        public string[] ReadResponse(Encoding enc, string prefix)
        {
            List<string> resp = new List<string>();

            string[] endTerm = new string[] { prefix + " OK", prefix + " NO", prefix + "BAD"};
            bool endOfResponse = false;

            while (!endOfResponse)
            {
                string response = ReadLine(enc);
                resp.Add(response);

                for (int nTerm=0; nTerm < endTerm.Length; nTerm++)
                {
                    if (response.StartsWith(endTerm[nTerm], StringComparison.InvariantCultureIgnoreCase))
                    {
                        endOfResponse = true;
                        break;
                    }
                }
            }
            return resp.ToArray();
        }


        public string ReadLine(Encoding enc)
        {
            string line = String.Empty;
            byte lastByte = 0x00;
            byte[] buffer = new byte[256];
            int nOffset = 0;
            while (true)
            {
                int nBytesRead = m_Ssl.Read(buffer, nOffset, 1);
                if (nBytesRead == 0)
                    throw new ApplicationException("Remote host disconnect");
                else
                {
                    if ((lastByte == 0x0D) && (buffer[nOffset] == 0x0A))
                    {
                        line += enc.GetString(buffer, 0, nOffset - 1);
                        break;
                    }
                    lastByte = buffer[nOffset];
                    nOffset += nBytesRead;
                    if (nOffset == buffer.Length)
                    {
                        line += enc.GetString(buffer, 0, buffer.Length);
                        buffer = new byte[512];
                        nOffset = 0;
                    }
                }
            }
            return line;
        }

        // Shuts down og closes connection
        public void Close()
        {
            if (m_Ssl != null)
                m_Ssl.Dispose();
            if (m_NetworkStream != null)
                m_NetworkStream.Dispose();
            if (m_Socket != null)
                m_Socket.Close();
        }
    }
}
