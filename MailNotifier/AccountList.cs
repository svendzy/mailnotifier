﻿using System;
using System.Collections.Generic;

namespace MailNotifier
{
    [Serializable()]
    public class AccountList : List<Account> {}
}
