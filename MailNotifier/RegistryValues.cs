﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;

namespace MailNotifier
{
    public static class RegistryValues
    {
        private static string MailNotifierKey = "HKEY_CURRENT_USER\\Software\\MailNotifier";

        public static bool IsFirstRun
        {
            get
            {
                object regValue = Registry.GetValue(MailNotifierKey, "_Runned", null);
                return (regValue == null) ? true : false;
            }
        }

        public static void SetFirstRun()
        {
            Registry.SetValue(MailNotifierKey, "_Runned", 1, RegistryValueKind.DWord);
        }

        public static byte[] SharedKey
        {
            get
            {
                object regValue = Registry.GetValue(MailNotifierKey, "_EncKey", null);
                return (byte[])regValue;
            }
            set
            {
                Registry.SetValue(MailNotifierKey, "_EncKey", value, RegistryValueKind.Binary);
            }
        }

        public static byte[] Salt
        {
            get
            {
                object regValue = Registry.GetValue(MailNotifierKey, "_Salt", null);
                return (byte[])regValue;
            }
            set
            {
                Registry.SetValue(MailNotifierKey, "_Salt", value, RegistryValueKind.Binary);
            }
        }
    }
}
