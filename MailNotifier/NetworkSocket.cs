using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace MailNotifier
{
    public class NetworkSocket
    {
        private Socket m_Socket;

        public NetworkSocket(string host, int port)
        {
            m_Socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            m_Socket.Connect(host, port);
        }

        public void WriteLine(string s, Encoding enc)
        {
            string str = s + "\r\n";
            byte[] buffer = enc.GetBytes(str);
            m_Socket.Send(buffer, SocketFlags.None);
        }

        public string ReadLine(Encoding enc)
        {
            string line = String.Empty;
            byte lastByte = 0x00;
            byte[] buffer = new byte[256];
            int nOffset = 0;
            while (true)
            {
                int nBytesRead = m_Socket.Receive(buffer, nOffset, 1, SocketFlags.None);
                if (nBytesRead == 0)
                    throw new ApplicationException("Remote host disconnect");
                else
                {
                    if ((lastByte == 0x0D) && (buffer[nOffset] == 0x0A))
                    {
                        line += enc.GetString(buffer, 0, nOffset - 1);
                        break;
                    }
                    lastByte = buffer[nOffset];
                    nOffset += nBytesRead;
                    if (nOffset == buffer.Length)
                    {
                        line += enc.GetString(buffer, 0, buffer.Length);
                        buffer = new byte[512];
                        nOffset = 0;
                    }
                }
            }
            return line;
        }

        public void Close()
        {
            if (m_Socket != null)
            {
                m_Socket.Shutdown(SocketShutdown.Both);
                m_Socket.Close();
            }
        }
    }
}
