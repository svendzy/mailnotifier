﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;

namespace MailNotifier
{
    public partial class FrmMain : Form, IObserver
    {
        private Config m_Config;
        private delegate void ChangeNotifyIconDelegate(Icon icon);
        private Dictionary<string, AccountStatus> statusList = new Dictionary<string, AccountStatus>();
        private bool soundPlayed = false;

        public FrmMain()
        {
            InitializeComponent();
            notifyIcon.Icon = Properties.Resources.ico_mail;
        }


        private void ChangeNotifyIcon(Icon icon)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new ChangeNotifyIconDelegate(ChangeNotifyIcon), new object[] { icon });
            }
            else
            {
                notifyIcon.Icon = icon;
            }
        }

        void IObserver.Notify(AccountStatus accountStatus)
        {
            Debug.WriteLine("OnNotify() " + accountStatus.Email + " at " + DateTime.Now);
            if (accountStatus.MessageCount > 0)
            {
                statusList[accountStatus.Email] = accountStatus;
                ChangeNotifyIcon(Properties.Resources.ico_mail_avail);
                
            }
            else
            {
                statusList.Remove(accountStatus.Email);
                if (statusList.Count == 0)
                    ChangeNotifyIcon(Properties.Resources.ico_mail);
            }

            if (statusList.Count > 0)
            {
                if (!soundPlayed)
                {
                    System.Media.SystemSounds.Exclamation.Play();
                    soundPlayed = true;
                }
            }
            else
            {
                soundPlayed = false;
            }
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FrmMain());
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            if (WindowState == FormWindowState.Minimized)
            {
                this.Hide();
                this.ShowInTaskbar = false;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // Check with registry whether application has runned before
            if (RegistryValues.IsFirstRun)
            {
                RegistryValues.SetFirstRun();

                byte[] saltValue = Encoding.ASCII.GetBytes("jA6EoV1NEv");
                RegistryValues.Salt = saltValue;

                byte[] sharedKey = new byte[20];
                RandomNumberGenerator rnd = RNGCryptoServiceProvider.Create();
                rnd.GetNonZeroBytes(sharedKey);
                byte[] enc_sharedKey = DataEncryptor.Encrypt(sharedKey);
                RegistryValues.SharedKey = enc_sharedKey;

                Crypto.SharedSecret = Convert.ToBase64String(sharedKey);
                Crypto.Salt = saltValue;
            }
            else
            {
                byte[] enc_sharedKey = RegistryValues.SharedKey;
                byte[] sharedKey = DataEncryptor.Decrypt(enc_sharedKey);

                Crypto.SharedSecret = Convert.ToBase64String(sharedKey);
                Crypto.Salt = RegistryValues.Salt;
            }

            m_Config = Config.Load();
            for (int nAccount = 0; nAccount < m_Config.Accounts.Count; nAccount++)
            {
                m_Config.Accounts[nAccount].SubscribeNotify(this);
                m_Config.Accounts[nAccount].StartNotify();
            }
        }


        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ToolStripItem item = e.ClickedItem;
            switch (item.Name)
            {
                case "mnuItemSetupAccount":
                    FrmAccountSetup frmSetupAccount = new FrmAccountSetup();
                    DialogResult result = frmSetupAccount.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        Account account = null;
                        switch (frmSetupAccount.AccountType)
                        {
                            case AccountType.Standard:
                                account = new StandardAccount();
                                break;
                            case AccountType.GMail:
                                account = new GMailAccount();
                                break;
                        }
                        account.Email = frmSetupAccount.Email;
                        account.Host = frmSetupAccount.Host;
                        account.Port = frmSetupAccount.Port;
                        account.Username = frmSetupAccount.Username;
                        account.Password = Crypto.EncryptStringAES(frmSetupAccount.Password);
                        account.CheckIntervalInMinutes = frmSetupAccount.CheckIntervalInMinutes;
                        m_Config.Accounts.Add(account);
                        m_Config.Save();

                        account.SubscribeNotify(this);
                        account.StartNotify();
                    }
                    break;
                case "mnuItemExit":
                    this.Close();
                    break;
            }
        }

        private void contextMenu_Opening(object sender, CancelEventArgs e)
        {
            if (m_Config != null)
            {
                if (m_Config.Accounts.Count == 0)
                {
                    mnuItemDeleteAccount.Visible = false;
                }
                else
                {
                    mnuItemDeleteAccount.Visible = true;
                    mnuItemDeleteAccount.DropDown.Items.Clear();
                    for (int nAccount = 0; nAccount < m_Config.Accounts.Count; nAccount++)
                    {
                        ToolStripMenuItem item = new ToolStripMenuItem();
                        item.Text = m_Config.Accounts[nAccount].Email;
                        item.Tag = nAccount;
                        mnuItemDeleteAccount.DropDown.Items.Add(item);
                    }
                }
            }
        }

        private void CheckDropdownMenuItem(ToolStripDropDown dropdown, string name)
        {
            for (int nItem = 0; nItem < dropdown.Items.Count; nItem++)
            {
                if (dropdown.Items[nItem].Name.Equals(name))
                    ((ToolStripMenuItem)dropdown.Items[nItem]).Checked = true;
                else
                    ((ToolStripMenuItem)dropdown.Items[nItem]).Checked = false;
            }
        }

        private void mnuItemDeleteAccount_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            int accountIndex = Convert.ToInt32(e.ClickedItem.Tag);
            m_Config.Accounts[accountIndex].StopNotify();
            m_Config.Accounts[accountIndex].Dispose();
            m_Config.Accounts.RemoveAt(accountIndex);
            m_Config.Save();
        }

        private void notifyIcon_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (statusList != null)
                {
                    if (statusList.Count > 0)
                    {
                        foreach (KeyValuePair<string, AccountStatus> entry in statusList)
                        {
                            AccountStatus accountStatus = entry.Value;
                            switch (accountStatus.AccountType)
                            {
                                case AccountType.Standard:
                                    DefaultMailClient.Run();
                                    break;
                                case AccountType.GMail:
                                    Process process = new Process();
                                    process.StartInfo.FileName = String.Format("https://www.google.com/accounts/ServiceLoginAuth?continue=http://mail.google.com/gmail&service=mail&Email={0}&PersistentCookie=No&null=Sign+in", accountStatus.Email);
                                    process.Start();
                                    break;
                            }
                        }
                    }
                }
            }
        }
    }
}
