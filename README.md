# Email-notifier with POP3 and IMAP support #

For demonstrational purposes only

![mailnotifier.jpg](https://bitbucket.org/repo/ao884b/images/1302647108-mailnotifier.jpg)

## Functions: ##
* Mangage one or more POP3 or IMAP-accounts
* Notifiy with sound and tray-icon when receiving new messages
* Automatically open assigned email-application

Techologies used: Visual Studio 2010, C#, Windows Forms, .Net Framework 2.0